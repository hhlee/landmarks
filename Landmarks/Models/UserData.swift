//
//  UserData.swift
//  Landmarks
//
//  Created by ibank on 2019/07/19.
//  Copyright © 2019 hhlee. All rights reserved.
//

import SwiftUI
import Combine

// BindableObject
// - 뷰모델(바인딩 가능 객체)
final class UserData: BindableObject {

    let didChange = PassthroughSubject<Void, Never>()

    var showFavoritesOnly = false {
        willSet {
            didChange.send()
        }
    }

    var landmarks = landmarkData {
        willSet {
            didChange.send()
        }
    }
}
