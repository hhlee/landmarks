//
//  LandmarkList.swift
//  Landmarks
//
//  Created by ibank on 2019/07/19.
//  Copyright © 2019 hhlee. All rights reserved.
//

import SwiftUI

struct LandmarkList : View {
    
    // @State (상태 속성)
    // - 상태 값이 변경되면 View 를 무효화하고 body 를 다시 그림
    // - 뷰의 내부 body(또는 View 에 의해 호출된 함수)에서만 상태 속성에 접근
    // - '$' 접두어를 붙여 바인딩 상태
    //@State var showFavoritesOnly = false
    
    // @EnvironmentObject
    // - BindableObject 값이 변경되면 View 를 무효화
    @EnvironmentObject private var userData: UserData
    
    var body: some View {
        
        NavigationView {
        
            List {
                
                Toggle(isOn: $userData.showFavoritesOnly) {
                    Text("Favorites only")
                }
                
                ForEach(userData.landmarks) { landmark in
                
                    if !self.userData.showFavoritesOnly || landmark.isFavorite {
                        NavigationLink(destination: LandmarkDetail(landmark: landmark)
                            .environmentObject(self.userData)) {
                            LandmarkRow(landmark: landmark)
                        }
                    }
                }
            }
            .navigationBarTitle(Text("Landmarks"))
        }
    }
}

#if DEBUG
struct LandmarkList_Previews : PreviewProvider {
    static var previews: some View {
        
        LandmarkList()
            .environmentObject(UserData())
    }
}
#endif
